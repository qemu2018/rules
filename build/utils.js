/*
 * This file is part of @adblockinc/rules <https://adblock.org/>,
 * Copyright (C) 2021-present Adblock Inc.
 *
 * @adblockinc/rules is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * @adblockinc/rules is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with @adblockinc/rules.  If not, see <http://www.gnu.org/licenses/>.
 */

import {spawn} from "child_process";

export async function execNpm(cmd, ...args)
{
  return new Promise((resolve, reject) =>
  {
    const proc = spawn(cmd, args, {shell:true});
    proc.on("close", (code) =>
    {
      if (code === 0)
      {
        resolve();
      }
      else
      {
        reject("Received error code");
      }
    });
    proc.on("error", reject);

    proc.stdout.on("data", (data) => console.log(data.toString()));
    proc.stderr.on("data", (data) => console.error(data.toString()));
  });
}
