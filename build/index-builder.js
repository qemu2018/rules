/*
 * This file is part of @adblockinc/rules <https://adblock.org/>,
 * Copyright (C) 2021-present Adblock Inc.
 *
 * @adblockinc/rules is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * @adblockinc/rules is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with @adblockinc/rules.  If not, see <http://www.gnu.org/licenses/>.
 */

import {promises as fs} from "fs";

import * as config from "./config/index.js";
import * as fsh from "./fs-helper.js";
import {execNpm} from "./utils.js";

export async function buildIndex(addonName)
{
  const availableSubscriptions = await fsh.readJson(
    await fsh.getDataPath(fsh.indexEyeo)
  );
  const subscriptionById = new Map(
    availableSubscriptions.map(
      (subscription) => [subscription.id, subscription]
    )
  );

  const recommendedSubscriptions = [];
  const recommendedSubscriptionIds = config[addonName];
  for (const id of recommendedSubscriptionIds)
  {
    const subscription = subscriptionById.get(id);
    if (!subscription)
      throw new Error(`Missing subscription: ${id}`);

    recommendedSubscriptions.push(subscription);
  }

  await fs.writeFile(
    await fsh.getDistPath(fsh.indexSelected),
    JSON.stringify(recommendedSubscriptions, null, 2),
    "utf8"
  );
  console.log("Created index", fsh.getRelativePath(fsh.indexSelected));

  return recommendedSubscriptions;
}

export async function updateIndex()
{
  try
  {
    await execNpm(
      "subs-init",
      "-o", await fsh.getDataPath(fsh.indexEyeo)
    );
    console.log("Updated index", fsh.getRelativePath(fsh.indexEyeo));
  }
  catch(ex)
  {
    // We'd like to keep the index file up-to-date, but may not be able to so, if
    // its format changed unexpectedly. Nevertheless, we can continue updating
    // subscriptions using the existing index.
    console.warn("Failed to update index:", ex);
  }
}
