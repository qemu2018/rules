#!/usr/bin/env node

/*
 * This file is part of @adblockinc/rules <https://adblock.org/>,
 * Copyright (C) 2021-present Adblock Inc.
 *
 * @adblockinc/rules is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * @adblockinc/rules is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with @adblockinc/rules.  If not, see <http://www.gnu.org/licenses/>.
 */

import {promises as fs} from "fs";
import path from "path";

import * as cli from "./cli.js";
import * as fsh from "./fs-helper.js";
import {buildIndex} from "./index-builder.js";
import {execNpm} from "./utils.js";

const addonName = cli.getAddonName();
fsh.setAddonName(addonName);

await fsh.clean();

const subscriptions = await buildIndex(addonName);

// subs-convert doesn't offer a way to specify which files we'd like to convert
// so we need to copy the relevant files to a temporary directory and pass
// that one instead
const dataRulesAbp = await fsh.getDataPath(fsh.rulesAbp);
const distRulesAbp = await fsh.getDistPath(fsh.rulesAbp);


await execNpm(
  "subs-convert",
  "-i", distRulesAbp,
  "-o", await fsh.getDistPath(fsh.rulesDnr)
);
console.log("Created rules", fsh.getRelativePath(fsh.rulesDnr));

await execNpm(
  "subs-generate",
  "-p", "/data/rules/dnr/",
  "-i", await fsh.getDistPath(fsh.rulesDnr),
  "-o", await fsh.getDistPath(fsh.manifestFragment)
);
console.log(
  "Created manifest fragment",
  fsh.getRelativePath(fsh.manifestFragment)
);
