/*
 * This file is part of @adblockinc/rules <https://adblock.org/>,
 * Copyright (C) 2021-present Adblock Inc.
 *
 * @adblockinc/rules is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * @adblockinc/rules is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with @adblockinc/rules.  If not, see <http://www.gnu.org/licenses/>.
 */

export {default as adblock} from "./adblock.js";
export {default as adblockplus} from "./adblockplus.js";
